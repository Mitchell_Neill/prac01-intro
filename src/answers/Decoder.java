package answers;

public class Decoder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* The secret message */
		final int[] message = { 82, 96, 103, 103, 27, 95, 106, 105, 96, 28 };
		
		/* The key for unlocking the message */
		final int key = 5;
		
		
		/* create an array of chars to store decrypted message */
		char[] decMessage = new char[message.length];

		
		/*
		/* Apply key to the message, and store output as char array */
		for( int i=0; i <message.length; i++){
			message[i] += key;
			decMessage[i] = (char)message[i];
		}
		
		/* print the decoded message */
		System.out.print(decMessage);
		
		


	}

}
